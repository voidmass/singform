SingWork Form jquery plugin
========

Plugin wrapping HTML Form handling for clientside. Provides validation of inputs, server-side only, realtime as user fill out form. And display relevant feedback and result messages
Library is not meant to be used for the general public. Only to be used with websites running SingWork


Features
--------

- Minimal config for standart forms
- Input validation performed only on server side, thus following same rules
- Allow for server to control action after successful submit (close dialog, reset form, or just password fields)
- Able to asynchronously upload files to server and attach result from server to eventual form submit


Use
------------
Include main library jQuery.form.js and jQuery.form.css (only required for default loaders and effects)

Then simply initialize the plugin on each form element as desired:

$("#myForm").singForm(options);

options are not required for default use but much if plugin behaviour can be customized as follows


Options
-------
- **reset:** *true / false* - whether to reset form after successful submit (default: true)(can be overriden from serer response)
- **uploadTrigger:** *submit / change* - when the attached files should be uploaded. Either on central form submit, or after file has been selected (default: submit)
- **submit_block:** *true / false* - If set to true, all submit buttons becomes disabled if there is one or more invalid input elements(default false)
- **check_on:** *blur / change* - When to send input value to server. Notice: when using change consider using check_timeout class to delay to response not to send data on every keystroke (default: blur)
- **loader:** *html* - HTML markup of loader to display when processing submit
- **callback:** *Object* - Possible callback are: error - call on error response on submit / submit - call on each submit / success - call on successful submit
- **handlers** *Object* - Handlers to provide styling effects and action for the form element possible values:
    - **fieldMessage** - used to display valid/invalid message around inputs
    - **submitStart** - called on start of a submit (to display loader, disable buttons etc.)
    - **submitResult** - called after EACH submit (hide loader, enable buttons)
    - **submitSuccess** - called after successful submit (clear invalid/valid messages from elements)
    - **actionClose** - closes Bootstrap or Foundation modal window, invoked based on server reponse
    - **anyAction** - any can be substituted with any word to create custom handlers for server action response


License
-------

The project is licensed under the BSD license.