(function ($) {
    $.fn.singForm = function (opts) {

        if ($(this).length === 0) {
            return;
        }
        var options = new Object();
        if (opts) {
            options = $.extend(true, $.fn.singForm.defaults, opts);
        } else {
            options = $.extend(true, {}, $.fn.singForm.defaults);
        }
        var self = $(this);
        if (!self.is('form')) {
            new Toast('Unknown error, please try again later', 5000);
            return false;
        }
        self.data('options', options);
        self.data('options').url = this.attr('action');
        this.off('submit').on('submit', submit);
        if (self.data('options').check_on == 'blur') {
            this.find('.check').on('blur', check);
        } else if (self.data('options').check_on == 'change') {
            this.find('.check').on('input propertychange paste', checkChange);
        }
        this.find('button.add').on('click', add);
        if (self.data('options').uploadTrigger === 'change') {
            this.find('input[type=file]').on('change', upload);
        }
        //$(document).on('dragover',prevent).on('drop',prevent);
        this.find('.file_pane.adder');
        this.find('[type=submit]').click(function () {
            $(this).closest("form").find('[type=submit]').removeAttr("clicked");
            $(this).attr("clicked", "true");
        });
        if (self.data('options').submit_block === true) {
            this.find('[type=submit]').attr('disabled', '');
            self.data('blocks', this.find('.check.required').length);
        }
        this.data('files', []);

    };

    $.fn.singForm.defaults = {
        callback: {
            error: function () {
            },
            submit: function (dataOutput, form) {
            },
            success: function (form, data, payload) {

            }

        },
        reset: true,
        uploadTrigger: 'submit', //  submit / change
        submit_block: false,
        check_on: 'blur', // blur / change
        loader: $('<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>'), // loader html to be added
        handlers: {
            fieldMessage: function (message, type, field, form) { //Errors/messages directed at specified input area
                var error = field.siblings('.form-error');
                if (type === 'invalid') {
                    field.closest('label').addClass('is-' + type + '-label');
                    field.addClass('is-' + type + '-input');
                    error.html(message);
                    error.addClass('is-visible');
                } else {
                    field.closest('label').removeClass('is-' + type + '-label');
                    field.removeClass('is-' + type + '-input');
                    error.removeClass('is-visible');
                    form.data('blocks', form.data('blocks') - 1);
                }
            },
            submitStart: function (form) { //displays loader on form submit
                var result = form.find('.singform-result');
                result.children().fadeOut(100, function () {
                    $(this).remove();
                });
                var loader = form.data('options').loader.clone();
                loader.hide();
                result.append(loader);
                loader.fadeIn(250);
            },
            /**
             * 
             * @param singForm form
             * @param String message
             * @param String type Can be success or error
             * @returns void
             */
            submitResult: function (form, message, type) {
                var result = form.find('.singform-result');
                if (message === undefined || message == '') {
                    result.children().fadeOut(300, function () {
                        $(this).remove();
                    });
                    return;
                }
                var messageBox = $('<div class="singform-message">' + message + '</div>');
                messageBox.addClass(type).css({opacity: 0});
                var loader = result.children();
                result.append(messageBox);
                messageBox.animate({opacity: 1}, 300);
                loader.animate({opacity: 0}, 300, function () {
                    $(this).remove();
                });
            },
            submitSuccess: function (form) {
                form.find('label.is-invalid-label').removeClass('is-invalid-label');
                form.find('input.is-invalid-input').removeClass('is-invalid-input');
                form.find('.form-error').removeClass('is-visible');
            },
            actionClose: function (form) {
                $('#modal').modal('hide');
            }
        }
    };
    $.fn.singFormFn = function () {
        var form = $(this);
        return {
            displayLoader: function () {
                submitStart(form);
            },
            displayResult: function () {
                submitResult(form, 'TEST', 'success');
            }
        }
    };
  
    function submitStart(form) {
//        form.find('.submit').data('old', form.find('.submit').html()).html('Zpracovávám');
        form.data('options').handlers.submitStart(form);
    }
    function submitSuccess(form) {
        form.data('options').handlers.submitSuccess(form);
    }
    /**
     * Callbacks change presubmit -> submit, submit -> success, error -> error
     * @param Event e
     * @returns void
     */
    function submit(e) {
        e.preventDefault();
        var form = $(e.delegateTarget);
        submitStart(form);
        var dataOutput = serialize(form);
        dataOutput.fields.submit = form.find("[type=submit][clicked=true]").val();        
        dataOutput.fields['_files'] = form.data('files');   
        form.data('options').callback.submit(dataOutput);
        form.trigger('singform:submit', [dataOutput]);
        var request = new Ajax(form.data('options').url, {type: 'save', fields: dataOutput.fields, files: form.data('files'), method: 'ajax'});
        request.success(function (payload) {
            submitSuccess(form);
            if (payload.data.status === 'valid') {
                submitResult(form, payload.data.data.message, 'success');
//                form.find('.status').html('');
                form.data('files', []);
                form.find('.files').html('');

                form.data('options').callback.success(form, payload.data, payload);
                form.trigger('singform:success', [payload]);
                if (payload.data.data.action !== undefined) {
                    processAction(payload.data.data.action, form);
                }
            } else {
                form.data('options').callback.error(form, payload.data, payload);
                form.trigger('singform:error', payload);
                for (var i in payload.data.fields) {
                    fieldMessage('invalid', payload.data.fields[i], form);
                }
                submitResult(form, payload.data.message, 'error');
            }
        });
        request.error(function (payload) {
            form.trigger('singform:ajax_error', payload);
            var message = payload.message;
            if (payload.error) {
                message += ' (' + payload.error + ')';
            }
            if (form.find('.result').length == 0) {
                new Toast(message, 5000);
            } else {
                submitResult(form, message, 'error');
            }
        });
        request.run();
    }
    /**
     * After succesful form submit proccesss response from server whether and what action to perform post submit
     * 
     * 
     * @param Object action Specification of action to be performed
     * @param jquery form form current form
     * @returns {undefined}
     */
    function processAction(action, form) {
        switch (action.type) {
            case 'redirect':
                window.top.location = action.data;
                break;
            case 'reset':
                form[0].reset();
                break;
            case 'reset_password':
                form.find('input[type=password]').val('');
                break;
            case 'close':
                form.data('options').handlers.actionClose(form);
                break;
            default:
                if (form.data('options').handlers['action'+action.type] !== undefined) {
                    form.data('options').handlers['action'+action.type](form);
                }
                break;
        }
    }
    /**
     * Uploades file/s to server and attaches the resulting response from server to future form submit event
     * @param jquery event e
     * @returns {undefined}
     */
    function upload(e) {
        var form = $(e.delegateTarget).closest('form');
        hideResult(form);
        form.find('input[type=submit],button[type=submit]').attr("disabled", "disabled");
        var data = new FormData();
        data.append($(e.delegateTarget).attr('name'), e.target.files[0]);
        data.append('fields[type]', 'file');
        $(e.delegateTarget).wrap('<form>').closest('form').get(0).reset();
        $(e.delegateTarget).unwrap();
        var progress = form.find('.progress');
        // progress.find('.meter').html('0%');
        progress.find('.meter').css('width', '0%');
        progress.animate({opacity: 1}, 250);
        var params = {
            cache: false,
            contentType: false,
            processData: false,
            xhr: function () {
                var req = $.ajaxSettings.xhr();
                if (req) {
                    req.upload.addEventListener('progress', function (ev) {
                        //progress.find('.meter').html(Math.round(ev.loaded * 100 / ev.total)+"%");                        
                        progress.find('.meter').css({width: Math.round(ev.loaded * 100 / ev.total) + "%"});
                    }, false);
                }
                return req;
            }
        };
        new Ajax(form.data('options').url, data, params).success(function (data) {
            form.find('.upload_append').remove();
            for (var i in data.data.files) {
                form.append($('<input type="hidden" name="' + i + '" class="upload_append" value="' + data.data.files[i] + '" />'));
            }
            if (data.data.thumbnail) {
                if ($(e.delegateTarget).parent().find('.thumbnail').length > 0) {
                    $(e.delegateTarget).parent().find('.thumbnail').attr('src', data.data.thumbnail);
                } else {
                    $(e.delegateTarget).after($('<img src="' + data.data.thumbnail + '" class="thumbnail form" />'));
                }

            }
            form.find('input[type=submit],button[type=submit]').removeAttr("disabled");
        }).error(function (data) {
            displayResult(form, data.message, 'error');
        }).complete(function () {
            //progress.find('.meter').html('100%');                        
            progress.find('.meter').css({width: "100%"});
            window.setTimeout(function () {
                progress.animate({opacity: 0}, 250);
            }, 4000);
        }).run();
    }
    /**
     * 
     * @param jquery form current form
     * @param String message message to be displayed
     * @param String type type of the message success/error
     * @returns {undefined}
     */
    function submitResult(form, message, type) {
        form.data('options').handlers.submitResult(form, message, type);
    }
    /**
     * Provides timeout on check function to send data to server with a delay
     * @param jquery event e
     * @returns {undefined}
     */
    function checkChange(e) {
        window.clearTimeout($(e.delegateTarget).closest('form').data('check_timeout'));
        $(e.delegateTarget).closest('form').data('check_timeout', window.setTimeout(function () {
            check(e);
        }, 700));
    }
    /**
     * Sends input value to server for evaulation
     * 
     * Display fieldMessage as a result
     * 
     * @param jquery event e
     * @returns {undefined}
     */
    function check(e) {
        // console.log('checking!');
        //$(e.delegateTarget).parent().removeClass('invalid').removeClass('valid');
        new Ajax($(e.delegateTarget).closest('form').data('options').url, {type: 'check', id: $(e.delegateTarget).attr('id'), value: $(e.delegateTarget).val(), method: 'ajax'})
                .success(function (data) {
                    fieldMessage(data.data.status, {id: $(e.delegateTarget).attr('id'), message: data.data.message}, $(e.delegateTarget).closest('form'));
                }).run();
    }
    /**
     * Finds specified input from field definition and calls fieldMessage calllback
     * @param String type type of the message valid/invalid/error
     * @param Array field specification on the field and its message
     * @param jQuery form current form element 
     * @returns {undefined}
     */
    function fieldMessage(type, field, form) {
        var el = $("[id='" + field.id + "']");
        form.data('options').handlers.fieldMessage(field.message, type, el, form);        
    }
    /**
     * 
     * @param jQuery form jquery object of the current form
     * @returns Array serialized array of values from form element
     */        
    function serialize(form) {
        var serializedArray = {};
        var inputs = form.serializeArray();
        $.each(inputs, function (i, input) {
            var value = input.value;
            var matches = input.name.match(/^([a-z0-9]+?)(\[.*\])?$/i);
            var mainKey = matches[1];
            if (matches[2] === undefined) {
                serializedArray[mainKey] = value;
            } else {
                matches = matches[2].match(/\[.*?\]/gi);
                if (matches) {
                    if (!(mainKey in serializedArray)) {
                        serializedArray[mainKey] = new Object();
                    }
                    var stack = serializedArray;
                    var cur = mainKey;
                    for (var i in matches) {
                        var c = matches[i].match(/^\[(.*)\]$/)[1];
                        if (c == "") {
                            if (Object.prototype.toString.call(stack[cur]) != '[object Array]') {
                                stack[cur] = new Array();
                            }
                            stack[cur].push(new Object());
                            stack = stack[cur];
                            cur = stack.length - 1;
                        } else {
                            if (!(c in stack[cur])) {
                                stack[cur][c] = new Object();
                            }
                            stack = stack[cur];
                            cur = c;
                        }
                    }
                    stack[cur] = value;

                }
            }
        });
        return serializedArray;
    }
}(jQuery));